/* eslint-disable linebreak-style */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable no-console */
/* eslint-disable max-classes-per-file */
var homyak;
var pass = false;
// Создадим базовый класс для всех хомяков
class Hamster {
  constructor() {
    this.name = 'Обычный';
    this.sound = 'средне';
    this.wheelspeed = '5 оборотов в минуту';
    this.food = ['Злаки', 'Пшено', 'Кукуруза', 'Семечки'];
  }

  whatfood() {
    // Тут должен быть return, но в консоль легче выводить так
    console.log(`${this.name} хомяк питается следующим: ${this.food}`);
  }

  howloud() {
    console.log(`${this.name} хомяк кричит ${this.sound}`);
  }

  howfast() {
    console.log(`${this.name} хомяк крутит колесо со скоростью ${this.wheelspeed}`);
  }
}
// Наследуем класс от хомяка обычного, только теперь пусть будет резиновый

class Rubber extends Hamster {
  constructor(sound) {
    super(sound);
    this.food = ['Клей момент', 'Вода дистиллированная'];
    this.wheelspeed = '0 оборотов в минуту';
    this.wornout = 'поношен довольно сильно';
    this.name = 'Резиновый с пищалкой';
  }

  // Новая функция, позволяет узнать насколько хомяк поношен
  howworn() {
    console.log(`${this.name} резиновый хомяк  ${this.wornout}`);
  }

  // Переопределим старую функцию. А точнее-дополним
  howfast() {
    super.howfast();
    console.log('... ну, это если резиновые хомяки вообще могут есть');
  }
}

class Angora extends Hamster {
  constructor(sound, wheelspeed) {
    super(sound, wheelspeed);
    this.food = ['KFC крылышки', 'Макдональдс', 'Кока-кола', 'Чипсы'];
    this.name = 'Ангорский';
  }

  // Полностью переопределяем родительский метод
  howfast() {
    console.log(`${this.name} хомяк не крутит колесо, потому что ему лень. Хотя технически он может`);
  }

  whatfood() {
    super.whatfood();
    console.log('... и в очень больших количествах');
  }
}
class Junharian extends Hamster {
  constructor(food, wheelspeed) {
    super(food, wheelspeed);
    this.name = 'Джунгарский';
    this.sound = 'слишком громко';
  }
}
const type = process.argv[2];
const action = process.argv[3];
// Это очень-очень плохой метод, но лучше чем try-catch и eval... наверное
switch (String(type)) { // Смотрим первый аргумент
  case 'common':
    homyak = new Hamster();
    pass = true;
    break;
  case 'angora':
    homyak = new Angora();
    pass = true;
    break;
  case 'rubber':
    homyak = new Rubber();
    pass = true;
    break;
  case 'junharian':
    homyak = new Junharian();
    pass = true;
    break;
  default:
    console.log('Таких хомяков в природе не существует!');
    break;
}

if (pass === true) {
  switch (String(action)) { // Смотрим второй аргумент
    case 'food':
      homyak.whatfood();
      break;
    case 'speed':
      homyak.howfast();
      break;
    case 'sound':
      homyak.howloud();
      break;
    case 'worn':
      homyak.howworn();
      break;
    default:
      console.log('Вы забыли указать действие или выбрали его неправильно');
  }
}
